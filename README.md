# Deviation
Repozytorium zawierające wszelką pracę możliwą do zapisu w formie cyfrowej zespołu Deviation.

## Po co to robić
W zespole mamy bardzo często do czynienia z sytuacjami, gdzie poszczególni członkowie są w posiadaniu przedawionych lub bezużytecznych informacji przekazywanych pocztą pantoflową. Aktulana praca zespołu powinna być dostępna dla każdego członka w jak najprostszy sposób, tak, aby sytuacje, kiedyś ktoś pracuje na starych tabach (lub narzeka że nie mógł się nauczyć bo nie miał tabów) albo uczy się nieaktualnych tekstów się już nigdy nie powtórzyły. 

## Struktura repozytorium
Główna ścieżka repozytorium zawiera katalogi nazwane po poszczególnych płytach zespołu lub ```in_progress```, jeżeli trwa praca nad nowym materiałem. 

## Struktura katalogu
Każdy katalog zawiera pięć podkatalogów (na chwilę obecną)
- **taby** - rozpisane tabulatory w formacie ```.gp5``` poszczególnych utworów
- **teksty** - teksty do poszczególnych utworów
- **studio** - pliki audio zmiennych formatów z nagraniami ze studia
- **proba** - pliki audio zmiennych formatów z nagraniami z prób
- **inne** - jak nazwa

## Jak pracować
Na chwilę obecną, cała praca zespołu jest w głównej gałęzi. Gałąź można pobrać skompresowaną w archiwum ```.zip``` klikając w ikonę ```Download``` obok kafelki ```Clone```. Zawartość archiwum będzie zawierała strukturę identyczną jak ta, która pojawia przy otwieraniu repozytorium w przeglądarce. W miarę postępów, repozytorium przejdzie restrukturyzację.

## Edycja
Repozytorium jest publiczne, aby każdy członek zespołu nie musiał przechodzić przez początkowo skomplikowany proces nauki systemu kontroli wersji. Jeżeli ktoś potrafi, to będzie wiedział jak wygląda proces edycji, jeżeli nie, to wszelkie rzeczy, które według powszechnej opinii powinny się tutaj znaleźć, proszę wysyłać na ```daniel.kopiecki@gmail.com```.