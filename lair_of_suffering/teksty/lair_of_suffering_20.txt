Constant pain, causes monotony,
this is the penance, for past mistakes,
escaping reality, in which I don't want to live,
will never, bring relief again

Lying,
embarrassed,
tortured by the needles of time,
filthy,
spirit,
is not an excuse,
tormented,
conscience,  
becomes the tormentor,
slaughtering my inner self

Act is always the same, it cannot be changed,
paralyzed by machine that is your own invention, 
needles begin their work, but you can't do anything to stop their craft,
precise process, carving the letters that form several sentences,
your body begins to sweat, but this is not a problem,
content of judgement will be read, from your own corpse

Lie down, in the lair of suffering

I don't want to wake up,
let me die in my sleep,
I don't want to lay down, 
to feel that pain, over and over again

Rusty needles working amongst the dark,
mental suffering, turned into physical pain,
imaginary torturer, calls me in my head,
by my name,
warm refugee, for me is torture chamber, 
no escape, from memories,
humanity, is a disease,
that i'm suffering through

I don't want to feel anything,
I don't want to feel jack shit,
I don't want to feek anything
I don't want to feel jack shit,
anymore
